window.addEventListener('DOMContentLoaded', () => {
	document.getElementById("btnGenerar").addEventListener('click', () => {
        // funcion promedio
		function promedio (array){
            let promedio = 0;
            for (let index = 0; index < array.length; index++) {
                promedio += array[index];
            }
            return promedio/array.length;
		}

		// funcion pares	
		function pares (array){
            let pares = 0;
            for (let index = 0; index < array.length; index++) {
                if (array[index] % 2 === 0) {
					pares++;
				}
                
            }
			return pares;
		}

		// funcion ordenar
		function ordenar (array){
			return array.sort(function(a, b) {return b - a});
		}


		let ArrayO = []; //var y let son similares
		for (let i = 0; i < 20; i++) {
			ArrayO.push(Math.floor(Math.random() * (100 - 1) + 1));
		}

		const rPromedio = promedio(ArrayO); // const es para dar valor y ya no se modificará
		const rPares = pares(ArrayO);
        arrayO = ordenar(ArrayO);
		document.getElementById('txtPromedio').value = rPromedio;
		document.getElementById('txtPares').value = rPares;

        // Impresion 
			let c = "";
			ArrayO.forEach(item => {
				c += `<p>${item}</p>\n`;
			});
			document.getElementById('ordenado').innerHTML = c;
	});
});